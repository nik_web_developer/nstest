<?php

namespace App\Controller;

use App\Entity\Nstest2;
use App\Form\Nstest2Type;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\Nstest2Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nstest2")
 */
class Nstest2Controller extends AbstractController
{
    /**
     * @Route("/", name="nstest2_index", methods={"GET"})
     */
    public function index(Nstest2Repository $nstest2Repository): Response
    {
        return $this->render('nstest2/index.html.twig', [
            'nstest2s' => $nstest2Repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="nstest2_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $nstest2 = new Nstest2();
        $form = $this->createForm(Nstest2Type::class, $nstest2);
        $form->handleRequest($request);

        $form = $this->createFormBuilder($nstest2)
            ->add('fname', TextType::class)
            ->add('lname', TextType::class)
            ->add('email', EmailType::class)
            ->add("country", CountryType::class, array(
                    "label" => "Country",
                    "required" => true,
            ))
            ->add('save', SubmitType::class, ['label' => 'Add Fields'])
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nstest2);
            $entityManager->flush();

            return $this->redirectToRoute('nstest2_index');
        }

        return $this->render('nstest2/new.html.twig', [
            'nstest2' => $nstest2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nstest2_show", methods={"GET"})
     */
    public function show(Nstest2 $nstest2): Response
    {
        return $this->render('nstest2/show.html.twig', [
            'nstest2' => $nstest2,
            'id' => $nstest2,
            'fname' => $nstest2,
            'lname' => $nstest2,
            'email' => $nstest2,
            'country' => $nstest2,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nstest2_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Nstest2 $nstest2): Response
    {
        $form = $this->createForm(Nstest2Type::class, $nstest2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nstest2_index', [
                'id' => $nstest2->getId(),
                /*'fname' => $nstest2->getFname(),
                'lname' => $nstest2->getLname(),
                'email' => $nstest2->getEmail(),
                'country' => $nstest2->getCountry(),*/
            ]);
        }

        return $this->render('nstest2/edit.html.twig', [
            'nstest2' => $nstest2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nstest2_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Nstest2 $nstest2): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nstest2->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($nstest2);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nstest2_index');
    }
}
