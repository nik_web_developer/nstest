<?php

namespace App\Repository;

use App\Entity\Nstest2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Nstest2|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nstest2|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nstest2[]    findAll()
 * @method Nstest2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Nstest2Repository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Nstest2::class);
    }

    // /**
    //  * @return Nstest2[] Returns an array of Nstest2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nstest2
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
