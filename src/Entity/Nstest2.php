<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Nstest2Repository")
 * @ORM\Table(name="nstest2")
 */
class Nstest2
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=200)
    */
    
    protected $fname;
    
    /**
     * @ORM\Column(type="string", length=200)
    */
    
    protected $lname;
    
    /**
     * @ORM\Column(type="string", length=200)
    */
    
    protected $email;
    
    /**
     * @ORM\Column(type="string", length=200)
    */
    
    protected $country;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getFname()
    {
        return $this->fname;
    }

    public function setFname($fname)
    {
        $this->fname = $fname;
    }

    public function getLname()
    {
        return $this->lname;
    }

    public function setLname($lname)
    {
        $this->lname = $lname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }
    
}
